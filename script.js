//Теоретичні питання
// 1) forEach - це метод масивів у JavaScript, який виконує певну функцію один раз для кожного елемента в масиві

// 2) Методи, які мутують (змінюють) існуючий масив: 
// push(), pop(), shift(), unshift(), splice(), sort(), reverse().
// Методи, які повертають новий масив: map(), filter(), slice(), concat().

// 3) Перевірка, чи змінна є масивом 
// Використання Array.isArray() 
let arr = [1, 2, 3];
console.log(Array.isArray(arr)); // поверне true
// Перевірка за допомогою методу instanceof
let arr2 = [1, 2, 3];
console.log(arr2 instanceof Array); // поверне true

// 4) Використовуйте метод map(), 
// коли вам потрібно створити новий масив на основі старого, 
// застосовуючи якусь логіку до кожного елемента масиву.
// Використовуйте метод forEach(), 
// коли потрібно просто виконати певні дії для кожного елемента масиву, 
// без створення нового масиву з результатами. 

// Практичні завдання 

// Завдання 1
let strings = ["travel", "hello", "eat", "ski", "lift"];
let count = 0;

for (let i = 0; i < strings.length; i++) {
    if (strings[i].length > 3) {
        count++;
    }
}

console.log("Кількість рядків з довжиною більше за 3 символи: " + count);

// Завдання 2
let people = [
    {name: "Іван", age: 25, sex: "чоловіча"},
    {name: "Марія", age: 30, sex: "жіноча"},
    {name: "Петро", age: 40, sex: "чоловіча"},
    {name: "Оксана", age: 35, sex: "жіноча"}
];

let man = people.filter(person => person.sex === "чоловіча");

console.log("Відфільтровані об'єкти зі статтю 'чоловіча':");
console.log(man);

// Завдання 3
function filterBy(array, dataType) {
    return array.filter(item => typeof item !== dataType);
}

let data = ['hello', 'world', 23, '23', null];
let filteredData = filterBy(data, 'string');
console.log(filteredData);